//
//  DashboardViewController.swift
//  Dirty Hands
//
//  Created by Manoj Ghale on 4/25/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

import UIKit
import Quickblox
import Fusuma
import SDWebImage
import UIImageView_Letters
import Haneke
import MBProgressHUD

class DashboardViewController: UIViewController, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, FusumaDelegate, UIScrollViewDelegate {
    var avatar: [String]?
    var userName: String!
    
    // Brand, Store, Time States
    var storeState: Int = 2
    var brandState: Int = 2
    var timeState:  Int = 1
    
    var cellUsername: String!
    var cellNeighbourhood: String!
    var cellState: String!
    
    var getObject: [AnyObject] = []
    var getCaption: [String] = []
    var getCreatedDate: [NSDate] = []
    var getUsername: [String] = []
    var getNeighbourhood: [String] = []
    var getState: [String] = []
    var getImage: [UIImage] = []
    var getBrand: [String] = []
    var getImageId: [UInt] = []
    
    // Objects
    var postFeeds:[AnyObject]?
    
    var getSortType: Bool = true
    
    @IBOutlet weak var feedListTableView: UITableView!
    @IBOutlet weak var commentUsername: UILabel!
    
    var arrCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        getDataFromServer()
        
        
        
        //        self.navigationController?.navigationBarHidden = true
        //        self.navigationItem.title = "Dirty Hands"
        self.navigationController?.navigationBar.backgroundColor = UIColor.grayColor()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        
        let rightBar: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Settings"), style: .Plain, target: self, action: #selector(DashboardViewController.settingsTapped))
        self.navigationItem.rightBarButtonItem = rightBar
        
        self.feedListTableView.delegate = self
        self.feedListTableView.dataSource = self
        //self.feedListTableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        getData()
        self.feedListTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func settingsTapped() {
        debugPrint("setting tapped")
    }
    
    @IBAction func cameraAction(sender: UIButton) {
        
        // Show Fusuma
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        FusumaMode.Camera
        self.presentViewController(fusuma, animated: true, completion: nil)
    }
    
    
    // MARK: - TableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows: Int = 0
        if(postFeeds != nil){
            numberOfRows = postFeeds!.count
        }
        return numberOfRows
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier: String = "dashboardCell"
        let cell: DashboardCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! DashboardCell
        let postFeed: AnyObject
        if getSortType == true {
            let inCount: Int = indexPath.row + 1
            let sequence: Int = ((self.postFeeds?.count)! - inCount)
             postFeed = postFeeds![sequence]
        } else {
            postFeed = postFeeds![indexPath.row]
        }
        
        if let fields: NSMutableDictionary = postFeed.fields{
            let baseURL: String = "https://qbprod.s3.amazonaws.com/"
            if let imageURLString: String = fields["imageURL"] as? String{
                let imageURL: NSURL = NSURL(string: "\(baseURL)\(imageURLString)")!
                SDWebImageManager.sharedManager().downloadImageWithURL(imageURL, options: SDWebImageOptions.ContinueInBackground, progress: { (completed: Int, total: Int) in
                    print(completed/total)
                    }, completed: { (image: UIImage!, error: NSError!, cache: SDImageCacheType, success: Bool, url: NSURL!) in
                        if(success){
                            cell.mainImage.image = image
                        }else{
                            print(error.localizedDescription)
                        }
                })
            }
            
            if let caption: String = fields["Caption"] as? String {
                cell.userComments.text = caption
            }
            
            if let brands: [String] = fields["Brand_selected"] as? [String] {
                print("Hello from brands:", brands)
                for brandId in brands {
                    QBRequest.objectWithClassName("Brand", ID: brandId, successBlock: { (response: QBResponse, object: QBCOCustomObject?) in
                        if let field: NSMutableDictionary = object?.fields {
                            let brandName: String = (field["brand_name"] as? String)!
                            print("Brand Selected:", brandName)
                            var frame: CGRect
                            for j in 0..<brands.count {
                                // buttonBrand: UIButton = UIButton()//(frame: CGRectMake(x, 5, 100, 30))
                                let buttonBrand: UIButton = UIButton()
                                if j == 0 {
                                    frame = CGRectMake(10, 5, 100, 30)
                                } else {
                                    frame = CGRectMake(CGFloat(j*100) + CGFloat(j*20) + 10, 5, 100, 30)
                                }
                                buttonBrand.frame = frame
                                buttonBrand.backgroundColor = UIColor(hexString: "#9B9B9B")
                                buttonBrand.tag = j
                                buttonBrand.setTitle(brandName, forState: UIControlState.Normal)
                                buttonBrand.backgroundColor = UIColor.grayColor()
                                buttonBrand.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                buttonBrand.layer.cornerRadius = 10
                                buttonBrand.clipsToBounds = true
                                
                                buttonBrand.titleLabel?.sizeToFit()
                                buttonBrand.titleLabel!.font =  UIFont.systemFontOfSize(12)
                                //                                        buttonBrand.titleLabel?.font.fontWithSize(9)
                                //buttonBrand.addTarget(self, action: #selector(PhotoUploadViewController.onClickofFilterBrand(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                                cell.showBrandScrollView.scrollEnabled = true
                                cell.showBrandScrollView.showsHorizontalScrollIndicator = false
                                cell.showBrandScrollView.indicatorStyle = .Default
                                cell.showBrandScrollView.contentSize = cell.showBrandScrollView.frame.size
                                cell.showBrandScrollView.addSubview(buttonBrand)
                            }

                        }
                        }, errorBlock: { (response: QBResponse) in
                            print(response.error?.description)
                    })
                    
                }
            }
            
            for feed in self.postFeeds! {
                
                if let userId: UInt = feed.userID as UInt {
                    QBRequest.userWithID(userId, successBlock: { (response: QBResponse, user: QBUUser?) in
//                        self.getUsername.append((user?.fullName)!)
                        print("Username\(user?.fullName)")
                        if let storeId: String = fields["Store_selected"] as? String {
                            QBRequest.objectWithClassName("Store", ID: storeId, successBlock: { (response:QBResponse, customObj:QBCOCustomObject?) in
                                print("customObj: \(customObj)")
                                if let fields: NSMutableDictionary = customObj?.fields {
                                    //NEIGHBORHOOD,STATE
                                    let neighborhood: String = (fields["Neighbourhood"] as? String)!
                                    let state: String = (fields["State"] as? String)!
                                    //cell.showStores.text = "@ " + neighborhood + ", " + state
                                    cell.username.text = (user?.fullName!)! + "@ " + neighborhood + ", " + state
                                    cell.interactorAvatar.setImageWithString(user?.fullName!, color: UIColor.grayColor(), circular: true)
                                    cell.commentUsername.text = (user?.fullName!)!
                                    
                                }
                                }, errorBlock: { (response: QBResponse) in
                                    print("error: \(response.error?.description)")
                            })
                            
                        }

//                        cell.username.text = user?.fullName!
//                        cell.interactorAvatar.setImageWithString(user?.fullName!, color: UIColor.grayColor(), circular: true)
                        }, errorBlock: { (errResponse:QBResponse) in
                            print(errResponse.error?.description)
                    })
                    
                }
            }

            
//            if let storeId: String = fields["Store_selected"] as? String {
//                QBRequest.objectWithClassName("Store", ID: storeId, successBlock: { (response:QBResponse, customObj:QBCOCustomObject?) in
//                    print("customObj: \(customObj)")
//                    if let fields: NSMutableDictionary = customObj?.fields {
//                        //NEIGHBORHOOD,STATE
//                        let neighborhood: String = (fields["Neighbourhood"] as? String)!
//                        let state: String = (fields["State"] as? String)!
//                       
//                        cell.showStores.text = "@ " + neighborhood + ", " + state
//                    }
//                    }, errorBlock: { (response: QBResponse) in
//                        print("error: \(response.error?.description)")
//                })
//                
//            }
        
        
        
        if let createDate: NSDate = postFeed.createdAt! {
            let formatterDate = NSDateFormatter.localizedStringFromDate(createDate, dateStyle: .LongStyle, timeStyle: .ShortStyle)
            cell.dateAndTime.text = formatterDate
        }

        }
            
        return cell
    }
    
   
    // MARK: - FUSUMA Delegate
    func fusumaImageSelected(image: UIImage) {
        
        print("Image selected")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // (Optional) Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage) {
        performSegueWithIdentifier("PhotoUpload", sender: image)
    }
    
    // (Optional) Called when the close button is pressed.
    func fusumaClosed() {
        
        print("Called when the close button is pressed")
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "PhotoUpload"){
            let photoUploadVC: PhotoUploadViewController = segue.destinationViewController as! PhotoUploadViewController
            let data: UIImage = sender as! UIImage
            photoUploadVC.image = data
        }
    }
    
    @IBAction func sortByStore(sender: AnyObject) {
        let getRequest: NSMutableDictionary = NSMutableDictionary()
        
        switch storeState {
        case 1:
            print("Ascending Order")
            let alert: UIAlertController = UIAlertController(title: "Sort", message: "Do you want to sort the content in by Store ascendingly?", preferredStyle: .Alert)
            let ok: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                print("Do Nothing")
            })
            alert.addAction(ok)
            self.presentViewController(alert, animated: true) {
                getRequest.setObject("Neighborhood_selected", forKey: "sort_asc")
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Sorting"
                
                QBRequest.objectsWithClassName("PostFeed", extendedRequest: getRequest, successBlock: { (response: QBResponse, cocObject:[QBCOCustomObject]?, responsePage: QBResponsePage?) in
                    print("CocObject\(cocObject)")
                    self.postFeeds = cocObject!
                    self.feedListTableView.reloadData()
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }) { (response: QBResponse) in
                    print(response)
                }
            }
            storeState = 2
            
        case 2:
            print("Descending Order")
            let alert: UIAlertController = UIAlertController(title: "Sort", message: "Are you want to sort the content by Store descendingly?", preferredStyle: .Alert)
            let ok: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                print("Do Nothing")
            })
            alert.addAction(ok)
            self.presentViewController(alert, animated: true) {
                getRequest.setObject("Neighborhood_selected", forKey: "sort_desc")
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Sorting"
                
                QBRequest.objectsWithClassName("PostFeed", extendedRequest: getRequest, successBlock: { (response: QBResponse, cocObject:[QBCOCustomObject]?, responsePage: QBResponsePage?) in
                    print("CocObject\(cocObject)")
                    self.postFeeds = cocObject!
                    self.feedListTableView.reloadData()
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }) { (response: QBResponse) in
                    print(response)
                }
            }
            
            storeState = 1
        default:
            print("Unwanted")
        }
        
    }
    
    @IBAction func sortByBrand(sender: AnyObject) {
        let getRequest: NSMutableDictionary = NSMutableDictionary()
        switch brandState {
        case 1:
            print("Brand Ascending Order")
            let alert: UIAlertController = UIAlertController(title: "Sort", message: "Are you want to sort the content by Brand?", preferredStyle: .Alert)
            let ok: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                print("Do Nothing")
            })
            alert.addAction(ok)
            self.presentViewController(alert, animated: true) {
                getRequest.setObject("Neighbourhood", forKey: "sort_asc")
            }
            brandState = 2
            
        case 2:
            print("Brand Descending Order")
            let alert: UIAlertController = UIAlertController(title: "Sort", message: "Are you want to sort the content by Brand?", preferredStyle: .Alert)
            let ok: UIAlertAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                print("Do Nothing")
            })
            alert.addAction(ok)
            self.presentViewController(alert, animated: true) {
                getRequest.setObject("Neighbourhood", forKey: "sort_desc")
            }

            brandState = 1
        default:
            print("Unwanted")
        }
        
        getRequest.setObject("Brand_selected", forKey: "sort_asc")
        QBRequest.objectsWithClassName("PostFeed", extendedRequest: getRequest, successBlock: { (response: QBResponse, cocObject:[QBCOCustomObject]?, responsePage: QBResponsePage?) in
            print("response: object: page \(response) \(cocObject) \(responsePage)")
        }) { (response: QBResponse) in
            print(response)
        }
    }
    
    func getData(){
        
        QBRequest.objectsWithClassName("PostFeed", successBlock: { (response: QBResponse, objects: [AnyObject]?) in
            self.postFeeds = objects
            self.feedListTableView.reloadData()
        }) { (error: QBResponse) in
                print("Error: \(error.error.debugDescription))")
            }
        }
}
