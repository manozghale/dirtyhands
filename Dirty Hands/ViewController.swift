//
//  ViewController.swift
//  Dirty Hands
//
//  Created by Manoj Ghale on 4/25/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

import UIKit
import Quickblox
import MBProgressHUD

class ViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var btmConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        self.navigationController?.navigationBar.translucent = false
//        btnSignIn.layer.cornerRadius = 5.0
//        btnSignIn.clipsToBounds = true
//        btnSignIn.backgroundColor = UIColor.blueColor()
        
        userView.layer.cornerRadius = 10.0
        userView.clipsToBounds = true
        userView.backgroundColor = UIColor.grayColor()
        
        passwordView.layer.cornerRadius = 10.0
        passwordView.clipsToBounds = true
        passwordView.backgroundColor = UIColor.grayColor()
        
        password.delegate = self
        userName.delegate = self
//        let pratik: String = "pratik.lohani@techguthi.com"
//        let binit: String = "binit.shrestha@techguthi.com"
//        let manoj: String = "techguthi"
        
        
//        userName.text = binit
//        password.text = "techguthi"
        
//        guard let currentUser = ServicesManager.instance().currentUser() else {
//            return
//        }
        
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
//        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    override func viewWillDisappear(animated: Bool) {
//        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
//        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
//
//    func adjustingHeight(show:Bool, notification:NSNotification) {
//        // 1
//        var userInfo = notification.userInfo!
//        // 2
//        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
//        // 3
//        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
//        // 4
//        let changeInHeight = (CGRectGetHeight(keyboardFrame) + 30) * (show ? 1 : -1)
//        //5
//        UIView.animateWithDuration(animationDurarion, animations: { () -> Void in
//            self.btmConstraint.constant += changeInHeight
//        })
//        
//    }
//    
//    func keyboardWillShow(notification:NSNotification) {
//        adjustingHeight(true, notification: notification)
//    }
//    
//    func keyboardWillHide(notification:NSNotification) {
//        adjustingHeight(false, notification: notification)
//    }
    
    @IBAction func signInProcessed(sender: AnyObject) {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Signing In"
        // login from quickblox
        let login: String = self.userName.text!
        let password: String = self.password.text!
        
        QBRequest.logInWithUserLogin(login, password: password, successBlock: { (response: QBResponse, error: QBUUser?) in
//            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let contorller: DashboardViewController = storyboard.instantiateViewControllerWithIdentifier("dashboard") as! DashboardViewController
//            self.navigationController?.pushViewController(contorller, animated: true)
            let user: String = (QBSession.currentSession().currentUser?.fullName)! as String
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            self.performSegueWithIdentifier("Dashboard", sender: user)
            
            
        }) { (error: QBResponse) in
                let alert = UIAlertController(title: "Error Login", message: "Enter correct username or password", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(defaultAction)
                self.presentViewController(alert, animated: true, completion: nil)
            
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        }

    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if(segue.identifier == "Dashboard"){
//            let userName: DashboardViewController = segue.destinationViewController as! DashboardViewController
//            let data: String = sender as! String
//            userName.userName = data
//            
//        }
//    }

    

}

