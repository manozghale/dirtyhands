//
//  AppDelegate.swift
//  Dirty Hands
//
//  Created by Manoj Ghale on 4/25/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

import UIKit
import Quickblox

let kQBApplicationID:UInt = 39933
let kQBAuthKey = "bthab8XGkvgxa54"
let kQBAuthSecret = "e6G6cRFcLDWBbek"
let kQBAccountKey = "oMrDkz7SxoeLsS9pNBP4"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        QBSettings.setApplicationID(kQBApplicationID)
        QBSettings.setAuthKey(kQBAuthKey)
        QBSettings.setAuthSecret(kQBAuthSecret)
        QBSettings.setAccountKey(kQBAccountKey)
        
        // enabling carbons for chat
        QBSettings.setCarbonsEnabled(true)
        
        // Enables Quickblox REST API calls debug console output.
        QBSettings.setLogLevel(QBLogLevel.Nothing)
        
        // Enables detailed XMPP logging in console output.
        QBSettings.enableXMPPLogging()
        QBSettings.setChatDNSLookupCacheEnabled(true)
        
        // If user is loggedin or not
        if (QBSession.currentSession().currentUser != nil) {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller: DashboardViewController = storyboard.instantiateViewControllerWithIdentifier("dashboard") as! DashboardViewController
            let navigationController: UINavigationController = UINavigationController(rootViewController: controller)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
//        signupUser()
        
        // UINavitionBar Color
//        var navigationBarAppearace = UINavigationBar.appearance()
//        navigationBarAppearace.tintColor = UIColor.whiteColor()
//        navigationBarAppearace.barTintColor = UIColor.lightGrayColor()

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func signupUser(){
        let user: QBUUser = QBUUser()
        user.password = "11111111"
        user.login = "manoj.ghale@techguthi.com"
        
        QBRequest.signUp(user, successBlock: { (response: QBResponse, user: QBUUser?) in
            print("Success Sign up")
        }) { (error: QBResponse) in
                print(error.data)
        }
    }

}

