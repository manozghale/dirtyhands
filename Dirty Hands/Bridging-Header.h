//
//  Bridging-Header.h
//  Dirty Hands
//
//  Created by Manoj Ghale on 5/2/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h

#import <Quickblox/Quickblox.h>
#import "DKScrollingTabController.h"
#import <UITextView_Placeholder/UITextView+Placeholder.h>
#import <MGInstagram/MGInstagram.h>

#endif /* Bridging_Header_h */
