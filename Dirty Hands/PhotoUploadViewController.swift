//
//  PhotoUploadViewController.swift
//  Dirty Hands
//
//  Created by Manoj Ghale on 4/27/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

import UIKit
import Quickblox
import MBProgressHUD
import SwiftHEXColors

class PhotoUploadViewController: UIViewController, UITextViewDelegate, UIScrollViewDelegate {
    var image: UIImage!
    
    // array to store the selected value in brand
    var brandSelectedArray: [String] = []
    let previousButton: UIButton = UIButton()
    // Bool status for save button
    var txtView: Bool = false
    var lblStore: Bool = false
    var lblBrand: Bool = false
    
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var storeName: UILabel!
    let controllerFlag: String = ""
    @IBOutlet weak var brandCaption: UITextView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var chooseStoreView: UIView!
    @IBOutlet weak var chooseBrandView: UIView!
    @IBOutlet weak var photoSection: UIView!
    @IBOutlet weak var storeSection: UIView!
    @IBOutlet weak var brandSection: UIView!
    let user1: Int = 12019966
    let user2: Int = 12600082
    let user3: Int = 12600100
    let user4: Int = 12600111
    let user5: Int = 12600128
    let user6: Int = 12600144
    
    @IBOutlet weak var saveState: UIButton!
    
    var stores: [AnyObject]?
    var brands: [AnyObject]?
    var selectedBrands: [AnyObject] = []
    var selectedStore: AnyObject?
    
    var Neighborhood_selected = ""
    
    
    var brandNameArray: [String] = []
    
    @IBOutlet weak var getBrandScrollView: UIScrollView!
    @IBOutlet weak var brandScrollView: UIScrollView!
    @IBOutlet weak var storeScrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getStores { (success) in
            if(success){
                var frame: CGRect = CGRect()
                var i: Int = 0
                for store in self.stores!{
                    if let fields: NSMutableDictionary = store.fields{
                        if let name: String = fields["Store_name"] as? String {
                            let buttonStore: UIButton = UIButton()//(frame: CGRectMake(x, 5, 100, 30))
                            if i == 0 {
                                frame = CGRectMake(10, 5, 150, 30)
                            } else {
                                frame = CGRectMake(CGFloat(i*150) + CGFloat(i*20) + 10, 5, 150, 30)
                            }
                            buttonStore.frame = frame
                            buttonStore.backgroundColor = UIColor(hexString: "#9B9B9B")
                            buttonStore.tag = i
                            buttonStore.setTitle(name, forState: UIControlState.Normal)
                            //button.backgroundColor = UIColor.grayColor()
                            buttonStore.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                            buttonStore.layer.cornerRadius = 10
                            buttonStore.clipsToBounds = true
                            buttonStore.titleLabel!.font =  UIFont.systemFontOfSize(12)
                            //if button.tag == i {
                            buttonStore.addTarget(self, action: #selector(PhotoUploadViewController.onClickofFilterStore(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                            //} else{
                            //    button.backgroundColor = UIColor(hexString: "#9B9B9B")
                            //}
                            
                            self.storeScrollView.addSubview(buttonStore)
                            
                            i += 1
                        }
                    }
                }
                
            }
        }
        
        getBrands { (success) in
            if(success){
                var frame: CGRect = CGRect()
                var i: Int = 0
                for brand in self.brands!{
                    if let fields: NSMutableDictionary = brand.fields{
                        if let name: String = fields["brand_name"] as? String {
                            // buttonBrand: UIButton = UIButton()//(frame: CGRectMake(x, 5, 100, 30))
                            let buttonBrand: UIButton = UIButton()
                            if i == 0 {
                                frame = CGRectMake(10, 5, 150, 30)
                            } else {
                                frame = CGRectMake(CGFloat(i*150) + CGFloat(i*20) + 10, 5, 150, 30)
                            }
                            buttonBrand.frame = frame
                            buttonBrand.backgroundColor = UIColor(hexString: "#9B9B9B")
                            buttonBrand.tag = i
                            buttonBrand.setTitle(name, forState: UIControlState.Normal)
                            buttonBrand.backgroundColor = UIColor.grayColor()
                            buttonBrand.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                            buttonBrand.layer.cornerRadius = 10
                            buttonBrand.clipsToBounds = true
                            buttonBrand.titleLabel!.font =  UIFont.systemFontOfSize(12)
                            buttonBrand.addTarget(self, action: #selector(PhotoUploadViewController.onClickofFilterBrand(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                            self.brandScrollView.addSubview(buttonBrand)
                            
                            i += 1
                        }
                    }
                }
                
            }
        }
        
        
        
        
        
        brandCaption.placeholder = "Insert Comment..."
        brandCaption.placeholderColor = UIColor.lightGrayColor() // optional
        checkStatus()
        
        // Navigation Bar Title
        //        self.navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: nil)
        //        self.navigationController?.navigationBarHidden = true
        //        self.navigationItem.title = "Tag A Photo"
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backButton")
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backButton")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)

        brandImage.image = image
        brandCaption.delegate = self
        
        storeScrollView.scrollEnabled = true
        brandScrollView.scrollEnabled = true
        getBrandScrollView.scrollEnabled = true
        
        storeScrollView.contentSize = storeScrollView.frame.size
        storeScrollView.showsHorizontalScrollIndicator = false
        storeScrollView.indicatorStyle = .Default
        
        brandScrollView.contentSize = brandScrollView.frame.size
        brandScrollView.showsHorizontalScrollIndicator = false
        brandScrollView.indicatorStyle = .Default
        //        getBrandScrollView.contentSize = brandScrollView.frame.size
        getBrandScrollView.showsHorizontalScrollIndicator = false
        getBrandScrollView.indicatorStyle = .Default
        
        //let x: CGFloat = 10
        /*
 var frame: CGRect
        var i = 0
        while i < store.count {
            let buttonStore: UIButton = UIButton()//(frame: CGRectMake(x, 5, 100, 30))
            if i == 0 {
                frame = CGRectMake(10, 5, 150, 30)
            } else {
                frame = CGRectMake(CGFloat(i*150) + CGFloat(i*20) + 10, 5, 150, 30)
            }
            buttonStore.frame = frame
            buttonStore.backgroundColor = UIColor(hexString: "#9B9B9B")
            buttonStore.tag = i
            buttonStore.setTitle(store[i], forState: UIControlState.Normal)
            //button.backgroundColor = UIColor.grayColor()
            buttonStore.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            buttonStore.layer.cornerRadius = 10
            buttonStore.clipsToBounds = true
            buttonStore.titleLabel!.font =  UIFont.systemFontOfSize(12)
            //if button.tag == i {
            buttonStore.addTarget(self, action: #selector(PhotoUploadViewController.onClickofFilterStore(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            //} else{
            //    button.backgroundColor = UIColor(hexString: "#9B9B9B")
            //}
            
            self.storeScrollView.addSubview(buttonStore)
            i += 1
        }
        
        for j in 0..<brand.count {
            // buttonBrand: UIButton = UIButton()//(frame: CGRectMake(x, 5, 100, 30))
            let buttonBrand: UIButton = UIButton()
            if j == 0 {
                frame = CGRectMake(10, 5, 150, 30)
            } else {
                frame = CGRectMake(CGFloat(j*150) + CGFloat(j*20) + 10, 5, 150, 30)
            }
            buttonBrand.frame = frame
            buttonBrand.backgroundColor = UIColor(hexString: "#9B9B9B")
            buttonBrand.tag = j
            buttonBrand.setTitle(brand[j], forState: UIControlState.Normal)
            buttonBrand.backgroundColor = UIColor.grayColor()
            buttonBrand.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            buttonBrand.layer.cornerRadius = 10
            buttonBrand.clipsToBounds = true
            buttonBrand.titleLabel!.font =  UIFont.systemFontOfSize(12)
            buttonBrand.addTarget(self, action: #selector(PhotoUploadViewController.onClickofFilterBrand(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.brandScrollView.addSubview(buttonBrand)
        }
 */
        
        // Creating border for UIViews
        self.viewBorderCustomize(brandSection)
        self.viewBorderCustomize(storeSection)
        self.viewBorderCustomize(photoSection)
        self.viewBorderCustomize(chooseBrandView)
        self.viewBorderCustomize(chooseStoreView)
        
        
        
    }
    
    func getStores(completionHandler:((success:Bool)->Void)){
        QBRequest.objectsWithClassName("Store", successBlock: { (response: QBResponse, objects: [AnyObject]?) in
            self.stores = objects
            completionHandler(success: true)
        }) { (error: QBResponse) in
            print("Error: \(error.error.debugDescription))")
            completionHandler(success: false)
        }
    }
    
    func getBrands(completionHandler:((success:Bool)->Void)){
        QBRequest.objectsWithClassName("Brand", successBlock: { (response: QBResponse, objects: [AnyObject]?) in
            self.brands = objects
            completionHandler(success: true)
        }) { (error: QBResponse) in
            print("Error: \(error.error.debugDescription))")
            completionHandler(success: false)
        }
    }
    
    func onClickofFilterStore(sender: UIButton) {
        for view in storeScrollView.subviews{
            if let button: UIButton = view as? UIButton{
                if button.tag == sender.tag {
                    sender.backgroundColor = UIColor(hexString: "#F5A623")
                } else {
                    button.backgroundColor = UIColor(hexString: "#9B9B9B")
                }
            }
        }
        
        if let store: AnyObject = stores![sender.tag]{
            if let fields: NSMutableDictionary = store.fields{
                if let name: String = fields["Store_name"] as? String {
                    self.storeName.text = name
                    self.selectedStore = store
                    if let neighborhood: String = fields["Neighbourhood"] as? String {
                        Neighborhood_selected = neighborhood
                    }
                }
            }
        }
        self.lblStore = true
        checkStatus()
    }
    
    func onClickofFilterBrand(sender: UIButton) {
        for view in brandScrollView.subviews {
            if let button: UIButton = view as? UIButton {
                if button.tag == sender.tag {
                    if button.backgroundColor == UIColor(hexString: "#4A90E2") {
                        button.backgroundColor = UIColor(hexString: "#9B9B9B")
                        if let brand: AnyObject = brands![sender.tag]{
                            if let fields: NSMutableDictionary = brand.fields{
                                if let name: String = fields["brand_name"] as? String {
                                    if(selectedBrands.contains({($0.fields!!["brand_name"]! as! String) == name})){
                                        if let itemToRemoveIndex = selectedBrands.indexOf({($0.fields!!["brand_name"]! as! String) == name}) {
                                            selectedBrands.removeAtIndex(itemToRemoveIndex)
                                            if selectedBrands.count == 0 {
                                                self.brandName.text = "Select A Brand"
                                            } else {
                                                var selectedBrandsName: [String] = []
                                                for brand in selectedBrands{
                                                    if let fields: NSMutableDictionary = brand.fields{
                                                        if let name: String = fields["brand_name"] as? String {
                                                            selectedBrandsName.append(name)
                                                        }
                                                    }
                                                }
                                                self.brandName.text =  selectedBrandsName.joinWithSeparator(",")
                                            }
                                        }
                                    }
                                    
                                    /* if brandNameArray.contains(name) {
                                        if let itemToRemoveIndex = brandNameArray.indexOf(brand[sender.tag]) {
                                            brandNameArray.removeAtIndex(itemToRemoveIndex)
                                            if brandNameArray.count == 0 {
                                                self.brandName.text = "Select A Brand"
                                            } else {
                                                for array in 0..<brandNameArray.count {
                                                    self.brandName.text =  brandNameArray.joinWithSeparator(",")
                                                }
                                            }
                                            
                                        }
                                    }
                                    */
                                    
                                }
                            }
                        }
                        
                    } else {
                        if self.brandName.text == "Select A Brand" {
                            button.backgroundColor = UIColor(hexString: "#4A90E2")
                            if let brand: AnyObject = brands![sender.tag]{
                                if let fields: NSMutableDictionary = brand.fields{
                                    if let name: String = fields["brand_name"] as? String {
                                        selectedBrands.append(brand)
                                        self.brandName.text = name
                                    }
                                }
                            }
                            
                        } else {
                            button.backgroundColor = UIColor(hexString: "#4A90E2")
                            
                            if let brand: AnyObject = brands![sender.tag]{
                                selectedBrands.append(brand)
                            }
                            
                            
                            var selectedBrandsName: [String] = []
                            for brand in selectedBrands{
                                if let fields: NSMutableDictionary = brand.fields{
                                    if let name: String = fields["brand_name"] as? String {
                                        selectedBrandsName.append(name)
                                    }
                                }
                            }
                            self.brandName.text =  selectedBrandsName.joinWithSeparator(",")
                            
//                            if let brand: AnyObject = brands![sender.tag]{
//                                if let fields: NSMutableDictionary = brand.fields{
//                                    if let name: String = fields["brand_name"] as? String {
//                                        selectedBrands.append(brand)
//                                    }
//                                }
//                            }
//                            for array in 0..<brandNameArray.count {
//                                self.brandName.text =  brandNameArray.joinWithSeparator(",")
//                                print(brandNameArray)
//                            }
                        }
                    }
                }
            }
        }
        
        self.lblBrand = true
        checkStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveContent(sender: AnyObject) {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Posting the content"
        
        let params: QBCOCustomObject = QBCOCustomObject()
        let imageData: NSData = UIImageJPEGRepresentation(brandImage.image!, 0.5)!
        
        QBRequest.TUploadFile(imageData, fileName: "plus", contentType: "image/jpeg", isPublic: true, successBlock: { (response: QBResponse, blob: QBCBlob) in
            params.className = "PostFeed"
            if let blobString: String = blob.publicUrl()?.characters.split("/").map(String.init).last?.characters.split(".").map(String.init).first{
                let finalURLComponent: String = blobString.characters.split(" ").map(String.init).joinWithSeparator("")
                params.fields!["imageURL"] = finalURLComponent
            }
            params.fields!["ImageId"]               = blob.ID
            params.fields!["Caption"]               = self.brandCaption.text
            params.fields!["Neighborhood_selected"] = self.Neighborhood_selected
            var brandIds: [String] = []
            for brand in self.selectedBrands{
                if let qbBrand: QBCOCustomObject = brand as? QBCOCustomObject{
                    if let brandId: String = qbBrand.ID{
                        brandIds.append(brandId)
                    }
                }
                
            }
            
            print(brandIds)
            
            if(!brandIds.isEmpty){
                params.fields!["Brand_selected"] = brandIds.joinWithSeparator(",")
            }
            
            if let qbSelectedStore: QBCOCustomObject = self.selectedStore as? QBCOCustomObject{
                if let storeId: String = qbSelectedStore.ID{
                    print(storeId)
                    params.fields!["Store_selected"] = storeId
                }
                
            }
   
            if QBSession.currentSession().currentUser != nil {
                QBRequest.createObject(params, successBlock: { (response, object) in
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    self.navigationController?.popViewControllerAnimated(true)
                    }, errorBlock: {(response: QBResponse) in
                        print("Response Error: ", response.error?.description)
                })
            } else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                print("No Current User")
            }
            }, statusBlock: { (request: QBRequest, status: QBRequestStatus?) in
                //print("Status: \(status?.percentOfCompletion)")
        }) { (response: QBResponse) in
            print("Error: \(response.error?.description)")
        }
        
        
    }
    
    func viewBorderCustomize(customView: UIView) {
        customView.layer.borderColor = UIColor.lightGrayColor().CGColor
        customView.layer.borderWidth = 1
        customView.clipsToBounds = true
    }
    
    @IBAction func clearAllText(sender: AnyObject) {
        self.brandCaption.text = ""
    }
    
    // UITextView Delegate
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        print("end editing")
        if !brandCaption.text.isEmpty {
            self.txtView = true
            checkStatus()
        } else {
            self.txtView = false
        }
    }
    
    // Check Save Button Visibility
    func checkStatus() {
        if (self.txtView && self.lblBrand && self.lblStore) {
            self.saveState.userInteractionEnabled = true
            self.saveState.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        } else {
            self.saveState.userInteractionEnabled = false
            self.saveState.setTitleColor(UIColor.brownColor(), forState: UIControlState.Normal)
        }
    }
    
    
    
}
