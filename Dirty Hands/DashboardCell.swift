//
//  DashboardCell.swift
//  Dirty Hands
//
//  Created by Manoj Ghale on 5/2/16.
//  Copyright © 2016 Techguthi. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {
    @IBOutlet weak var interactorAvatar: UIImageView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var showBrandView: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    @IBOutlet weak var commentUsername: UILabel!
    @IBOutlet weak var userComments: UILabel!
    @IBOutlet weak var showBrandScrollView: UIScrollView!
    @IBOutlet weak var showStores: UILabel!
    @IBOutlet weak var usernameTrailing: NSLayoutConstraint!

}
